# from logging import error
# import pandas as pd
import functions as cn
# from matplotlib import interactive
import matplotlib.pyplot as plt
import numpy as np

# Defining the data
row = 998                       # density of water
g = 9.81                        # acceleration due to gravity
Q = np.arange(1, 10.1, 0.1)     # Discharge in the main channel
Y = 0.35                        # height of Bazin Weir
b1 = 7                          # Width of the main channel
b2 = 2                          # Width of the second channel
b3 = 5                          # Width of the third channel
Sb = 0.0006                     # Bed slope
ks1 = 55.55                     # Strickler's coefficient for channel 1
ks2 = 45.45                     # Strickler's coefficient for channel 2
ks3 = 52.63                     # Strickler's coefficient for channel 3
L2 = 100                        # location of Bazin Weir
dQ = 0.001                      # will be used to decrease the ratio of Q2
Cd = 0.611                      # coefficient of Discharge
part3 = 9.0

# Calling the functions
area1, wperimeter1, hydraulic_radius1, hydraulic_depth1, depth_centroid1 = cn.rectangular_geometry(b1)
area2, wperimeter2, hydraulic_radius2, hydraulic_depth2, depth_centroid2 = cn.rectangular_geometry(b2)
area3, wperimeter3, hydraulic_radius3, hydraulic_depth3, depth_centroid3 = cn.rectangular_geometry(b3)

velocity1, discharge1 = cn.Q_chezy(area1, hydraulic_radius1, ks1, Sb)
velocity2, discharge2 = cn.Q_chezy(area2, hydraulic_radius2, ks2, Sb)
velocity3, discharge3 = cn.Q_chezy(area3, hydraulic_radius3, ks3, Sb)

fQi = 0.4
fQ = fQi
dx = 0.1
tolE = 0.002
tolP = 0.01
j = 0
fQ1 = np.empty(0)
h02 = np.empty(0)
hc2 = np.empty(0)
h03 = np.empty(0)
hc3 = np.empty(0)
h01 = np.empty(0)
hc1 = np.empty(0)
h2_bw = np.empty(0)
ratio = np.empty(0)
Q2 = np.array([round(Q[j]*fQ,4)])
Q3 = np.array([round(Q[j]-Q2[j],4)])
# Q3 = np.array(round(Q[j]-Q2[j],4),ndmin=1)
# Q2 = np.array(round(Q[j]*fQ,4),ndmin=1)
# Q2 = np.empty(0)
# Q2 = np.append(Q2,round(Q[j]*fQ,4))
# Q3 = np.empty(0)
# Q3 = np.append(Q3, round(Q[j]-Q2[j],4))
c = j+1 # counter used for appending values to Q2, Q3
steep_slope = False # to check and stop the procedure whenever we encounter a steep slope case

while steep_slope == False:
    while j < len(Q):
        continua = 1
        print(Q[j])
        while continua == 1:
            if j == c:
                c = c+1
                Q2 = np.append(Q2,round(Q[j] * fQ,4))
                Q3 = np.append(Q3,round(Q[j] - Q2[j],4))
            else:
                Q2[j] = round(Q[j]*fQ,4)
                Q3[j] = round(Q[j]-Q2[j],4)

            h2_bw = np.append(h2_bw, round(Y + ((3 / 2) * Q2[j] / (Cd * b2 * (2 * g) ** 0.5)) ** (2 / 3), 4))
            f_h02c = lambda h: discharge2(h) - Q2[j]
            h02c = round(cn.newtonR(f_h02c, 1),4)
            hc2c = round((Q2[j] ** 2 / g / b2 ** 2) ** (1 / 3),4)
            if h02c > hc2c:
                Sf2 = cn.energy_slope(area2, hydraulic_radius2, ks2, Q2[j])
                Fr2 = cn.froude(area2, hydraulic_depth2, Q2[j])
                i = 0
                # x2 = np.empty(0)
                # z2 = np.empty(0)
                # h2 = np.empty(0)
                # x2 = np.append(x2, L2)
                # h2 = np.append(h2, h2_bw[-1])
                # z2 = np.append(z2, -L2 * Sb)
                x2 = np.array([L2])
                h2 = np.array([h2_bw[-1]])
                z2 = np.array([-L2 * Sb])
                while x2[-1] > 0:
                    x2 = np.append(x2, x2[-1] - dx)
                    z2 = np.append(z2, z2[-1] + dx * Sb)
                    h2 = np.append(h2, h2[-1] - (Sb - round(Sf2(h2[-1]),4)) / (1 - round(Fr2(h2[-1]) ** 2,4)) * dx)
                    # h2 = np.append(h2, h2[-1] - (Sb - Sf2(h2[-1])) / (1 -Fr2(h2[-1]) ** 2) * dx)
                    i = i + 1
            else:
                print("Channel 2 is Steep Slope, Change Procedure")
                steep_slope=True
                break
                # error("Steep slope channel, change procedure")

            f_h03c = lambda h: discharge3(h) - Q3[j]
            h03c = round(cn.newtonR(f_h03c, 1),4)
            hc3c = round((Q3[j] ** 2 / g / b3 ** 2) ** (1 / 3),4)
            if h03c > hc3c:
                Sf3 = cn.energy_slope(area3, hydraulic_radius3, ks3, Q3[j])
                Fr3 = cn.froude(area3, hydraulic_depth3, Q3[j])
                i = 0
                # x3 = np.empty(0)
                # z3 = np.empty(0)
                # h3 = np.empty(0)
                # x3 = np.append(x3, L2)
                # h3 = np.append(h3, h03c)
                # z3 = np.append(z3, -L2 * Sb)
                x3 = np.array([L2])
                h3 = np.array([h03c])
                z3 = np.array([-L2 * Sb])
                while x3[-1] > 0:
                    x3 = np.append(x3, x3[-1] - dx)
                    z3 = np.append(z3, z3[-1] + dx * Sb)
                    h3 = np.append(h3, h3[-1] - dx * (Sb - round(Sf3(h3[-1]),4) / (1 - round(Fr3(h3[-1]) ** 2,4))))
                    # h3 = np.append(h3, h3[-1] - dx * (Sb - Sf3(h3[-1])) / (1 - Fr3(h3[-1]) ** 2))
                    i = i + 1
            else:
                print("Channel 3 is Steep Slope, Change Procedure")
                steep_slope=True
                break
                # error("Steep slope channel, change procedure")

            piezometric_head2, velocity_head2, E2 = cn.energy(area2, Q2[j])
            piezometric_head3, velocity_head3, E3 = cn.energy(area3, Q3[j])
            if abs(E3(h3[-1]) - E2(h2[-1])) / (E3(h3[-1])) > tolE:
                if E3(h3[-1]) > (E2(h2[-1])):
                    fQ = fQ + dQ
                    del x2,z2,h2,x3,z3,h3
                else:
                    fQ = fQ - dQ
                    del x2,z2,h2,x3,z3,h3
            else:
                continua = 0
                fQ1 = np.append(fQ1, fQ)

                f_h01i = lambda h: discharge1(h) - Q[j]
                h01i = round(cn.newtonR(f_h01i, 1),4)
                h01 = np.append(h01, h01i)
                hc1i = round((Q[j] ** 2 / g / b1 ** 2) ** (1 / 3),4)
                hc1 = np.append(hc1, hc1i)
                ratio = np.append(ratio, Q2[j] / Q3[j])

                f_h02i = lambda h: discharge2(h) - Q2[j]
                h02i = round(cn.newtonR(f_h02i, 1),4)
                h02 = np.append(h02, h02i)
                hc2i = round((Q2[j] ** 2 / g / b2 ** 2) ** (1 / 3),4)
                hc2 = np.append(hc2, hc2i)

                f_h03i = lambda h: discharge3(h) - Q3[j]
                h03i = round(cn.newtonR(f_h03i, 1),4)
                h03 = np.append(h03, h03i)
                hc3i = round((Q3[j] ** 2 / g / b3 ** 2) ** (1 / 3),4)
                hc3 = np.append(hc3, hc3i)

        if steep_slope==True:
            break
        if round(Q[j],2) == part3:
        #end of while continua==1
                w = j # variable utilised for plotting the profile when Q = 9 mc/s
                '''The profiles of channel 2 and channel 3 are stored in x2, z2, h2 and x3, z3, h3
                 respectively after numerous iterations for Q = 9 satisfyting the condition
                 of energy balance. Hence, these values are stored in new arrays and plotted
                 at the end to maintain the continuity of the program'''

                # for channel 2
                Sf2_p3 = cn.energy_slope(area2, hydraulic_radius2, ks2, Q2[j])
                Fr2_p3 = cn.froude(area2, hydraulic_depth2, Q2[j])
                i = 0
                # x2_p3 = np.empty(0)
                # z2_p3 = np.empty(0)
                # h2_p3 = np.empty(0)
                x2_p3 = x2
                h2_p3 = h2
                z2_p3 = z2

                # for channel 3
                Sf3_p3 = cn.energy_slope(area3, hydraulic_radius3, ks3, Q3[j])
                Fr3_p3 = cn.froude(area3, hydraulic_depth3, Q3[j])
                i = 0
                # x3_p3 = np.empty(0)
                # z3_p3 = np.empty(0)
                # h3_p3 = np.empty(0)
                x3_p3 = x3
                h3_p3 = h3
                z3_p3 = z3

                # for channel 1
                # height of water downstream the channel 1 is characterised by
                # the depths of water from channel 2 and channel 3 as E1=E2=E3
                piezometric_head2_1, velocity_head2_1, E2_1 = cn.energy(area2, Q2[j])
                piezometric_head3_1, velocity_head3_1, E3_1 = cn.energy(area3, Q3[j])
                piezometric_head1, velocity_head1, E1 = cn.energy(area1, Q[j])
                # Depth of water in channel arising From channel 2
                f_h12 = lambda h: (E2_1(h2[-1]) - E1(h))
                h12 = cn.newtonR(f_h12, 1)
                # Depth of water in channel arising from channel 3
                f_h13 = lambda h: (E3_1(h3[-1]) - E1(h))
                h13 = cn.newtonR(f_h13, 1)
                # Computing to restore normal depth in channel 1 due to channel 2
                Sf1_p3 = cn.energy_slope(area1, hydraulic_radius1, ks1, Q[j])
                Fr1_p3 = cn.froude(area1, hydraulic_depth1, Q[j])
                i = 0
                # x12_p3 = np.empty(0)
                # z12_p3 = np.empty(0)
                # h12_p3 = np.empty(0)
                # x12_p3 = np.append(x12_p3, 0)
                # h12_p3 = np.append(h12_p3, h12)
                # z12_p3 = np.append(z12_p3, 0)
                x12_p3 = np.array([0])
                h12_p3 = np.array([h12])
                z12_p3 = np.array([0])
                while abs(h12_p3[-1] - h01[-1]) / h12_p3[-1] > tolP:
                    x12_p3 = np.append(x12_p3, x12_p3[-1] - dx)
                    z12_p3 = np.append(z12_p3, z12_p3[-1] + dx * Sb)
                    h12_p3 = np.append(h12_p3, h12_p3[-1] - (Sb - Sf1_p3(h12_p3[-1])) / (1 - Fr1_p3(h12_p3[-1]) ** 2) * dx)
                    # h12_p3 = np.append(h12_p3, h12_p3[-1] - (Sb - round(Sf1_p3(h12_p3[-1]),4) / (1 - round(Fr1_p3(h12_p3[-1]) ** 2,4)) * dx))
                    # h12_p3 = np.append(h12_p3, h12_p3[-1] - (Sb - Sf1_p3(h12_p3[-1]) / (1 - Fr1_p3(h12_p3[-1]) ** 2 * dx)))
                    i = i + 1
                # Computing to restore normal depth in channel 1 due to channel 3
                i = 0
                # x13_p3 = np.empty(0)
                # z13_p3 = np.empty(0)
                # h13_p3 = np.empty(0)
                # x13_p3 = np.append(x13_p3, 0)
                # h13_p3 = np.append(h13_p3, h13)
                # z13_p3 = np.append(z13_p3, 0)
                x13_p3 = np.array([0])
                h13_p3 = np.array([h13])
                z13_p3 = np.array([0])
                while abs(h13_p3[-1] - h01[-1]) / h13_p3[-1] > tolP:
                    x13_p3 = np.append(x13_p3, x13_p3[-1] - dx)
                    z13_p3 = np.append(z13_p3, z13_p3[-1] + dx * Sb)
                    h13_p3 = np.append(h13_p3, h13_p3[-1] - (Sb - Sf1_p3(h13_p3[-1])) / (1 - Fr1_p3(h13_p3[-1]) ** 2) * dx)
                    # h13_p3 = np.append(h13_p3, h13_p3[-1] - (Sb - round(Sf1_p3(h13_p3[-1]),4) / (1 - round(Fr1_p3(h13_p3[-1]) ** 2,4)) * dx))
                    i = i + 1

        #end of if Q[j]==part3
        j += 1
        #end of while continua==1
    if steep_slope==True:
        break
    # Exporting the variables into an excel file
    # col1 = "Q"
    # col2 = "Q2"
    # col3 = "Q3"
    # col4 = "h01"
    # col5 = "h02"
    # col6 = "h03"
    # col7 = "hc1"
    # col8 = "hc2"
    # col9 = "hc3"
    # col10="fQ"
    # col11 = "ratio"
    # # col11 = "h2 last"
    # data = pd.DataFrame({col1:Q,col2:Q2,col3:Q3,col4:h01,col5:h02,col6:h03,col7:hc1,col8:hc2,col9:hc3,col10:fQ,col11:ratio})
    # data.to_excel('project_data3.xlsx', sheet_name='sheet1', index=False)

    print("\nFor Q = 9 mc/s")
    print("Normal depth in channel 1 is", h01[w], "m")
    print("Normal depth in channel 2 is", h02[w], "m")
    print("Normal depth in channel 3 is", h03[w], "m")
    print("Critical depth in channel 1 is", hc1[w], "m")
    print("Critical depth in channel 2 is", hc2[w], "m")
    print("Critical depth in channel 3 is", hc3[w], "m")
    print("Discharge flowing in channel 2 is", Q2[w], "mc/s")
    print("Discharge flowing in channel 3 is", Q3[w], "mc/s")
    print("Discharge ratio btw the channels is\n", ratio[w],"\n.")

    #Plotting the values
    plt.figure(1)
    plt.plot(x2_p3, z2_p3, color='k', linewidth=0.8)
    plt.plot(x2_p3, z2_p3 + hc2[w], '-', dashes=[8, 4, 8, 4, 8, 4], color='k', linewidth=0.8)
    plt.plot(x2_p3, z2_p3 + h02[w], '-', dashes=[8, 4, 8, 4, 8, 4], color='b', linewidth=1)
    plt.plot(x2_p3, z2_p3 + h2_p3, color='g', linewidth=2)

    plt.plot(x3_p3, z3_p3, color='k', linewidth=0.8)
    plt.plot(x3_p3, z3_p3 + hc3[w], '-', dashes=[8, 4, 8, 4, 8, 4], color='k', linewidth=0.8)
    plt.plot(x3_p3, z3_p3 + h03[w], '-', dashes=[8, 4, 8, 4, 8, 4], color='b', linewidth=1)
    plt.plot(x3_p3, z3_p3 + h3_p3, color='y', linewidth=2)

    plt.plot(x12_p3, z12_p3, color='k', linewidth=0.8)
    plt.plot(x13_p3, z13_p3, color='k', linewidth=0.8)
    plt.plot(x12_p3, z12_p3 + hc1[w], color='k',linewidth=0.8)
    plt.plot(x13_p3, z13_p3 + hc1[w], color='k', linewidth=0.8)
    plt.plot(x12_p3, z12_p3 + h12_p3, 'r', linewidth=1)
    plt.plot(x13_p3, z13_p3 + h13_p3, 'g', linewidth=1)
    plt.plot(x12_p3, z12_p3 + h01[w], 'b', linewidth=1)
    plt.plot(x13_p3, z13_p3 + h01[w], 'b', linewidth=1)
    # interactive(True)
    # plt.show()
    plt.figure(2)
    plt.plot(Q, ratio)
    # plt.show()
    plt.figure(3)
    plt.plot(Q,h01)
    plt.plot(Q,hc1)
    # plt.show()
    plt.figure(4)
    plt.plot(Q2,hc2)
    plt.plot(Q2,h02)
    # plt.show()
    plt.figure(5)
    plt.plot(Q3,hc3)
    plt.plot(Q3,h03)
    # interactive(False)
    plt.show()
    break
#end of while steep_slope==0

