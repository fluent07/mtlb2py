import numpy as np
import matplotlib.pyplot as plt
import test as cn

# Defining the data
g = 9.806
rho = 998
Ym = 3
Yv = 1.54
psi = 0.2
B = 3
ks = 65
Sb = 0.0005
L = 1000

area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid = cn.rectangular_geometry(B)

velocity, discharge = cn.Q_chezy(area, hydraulic_radius, ks, Sb)
f_h01 = lambda h: Ym - h - (velocity(h) ** 2 * (1 + psi) / (2 * g))
h01 = cn.newtonR(f_h01, Ym)
print("\n\nNormal depth in the channel is", h01, )
Q01 = discharge(h01)
print("Discharge in the channel is", Q01)

fr = cn.froude(area, hydraulic_depth, Q01)
f_hc1 = lambda h: fr(h) - 1
hc1 = cn.newtonR(f_hc1, 2)
print("Critical Depth of the channel is", hc1)

if hc1 > h01:
    print("\nSteep slope channel, Change Procedure")
else:
    print("\nMild Slope channel, Procedure OK\n\n")

continua = 1
Q = Q01
dQ = 0.1
tolE = 0.01
# nmb = 0

while continua == 1:
    frc = cn.froude(area, hydraulic_depth, Q)
    f_hc = lambda h: frc(h) - 1
    hc = cn.newtonR(f_hc, 2)
    f_h0 = lambda h: Q - discharge(h)
    h0 = cn.newtonR(f_h0, 1)

    if hc > h0:
        print("Steep slope channel, Modify the script")

    else:
        dx = 0.1
        
        x1 = np.empty(0)
        x1 = np.append(x1, L)
        z1 = np.empty(0)
        z1 = np.append(z1, 0)
        h1 = np.empty(0)

        if Yv > hc:
            h1 = np.append(h1, Yv)

        else:
            h1 = np.append(h1, 1.01 * hc)

        # print(h1)
        # Integration of the profile
        Sf = cn.energy_slope(area, hydraulic_radius, ks, Q)
        i = 0
        while x1[-1] > 0:
            x1 = np.append(x1, x1[-1] - dx)
            z1 = np.append(z1, z1[-1] + dx * Sb)
            h1 = np.append(h1, h1[-1] - (Sb - Sf(h1[-1])) / (1 - fr(h1[-1]) ** 2) * dx)
            i = i + 1
        
        energy1 = h1[-1] + Q ** 2 / 2 / g / area(h1[-1]) ** 2 * (1 + psi)
        if abs(Ym - energy1) / Ym > tolE:
            if energy1 > Ym:
                Q = Q - dQ
                # nmb=1
                # del x1,z1,h1
            else:
                Q = Q + dQ
                # nmb=1
                # del x1,z1,h1
                # nmb = 1
        else:
            continua = 0
            print("Discharge in the channel is..", Q)
            print("Depth at the Inlet of the channel is..", h1[-1])
            plt.plot(x1, z1 + hc, '-', dashes=[15, 4, 15, 4, 15, 4], linewidth=0.8, color='k')
            plt.plot(x1, z1, color='k')
            plt.plot(x1, z1 + h01, '--', dashes=[15, 4, 15, 4, 15, 4], linewidth=0.8, color='b')
            plt.plot(x1, z1 + h1, color='r')
            plt.show()