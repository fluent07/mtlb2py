import numpy as np
import matplotlib.pyplot as plt
import test as cn


##Defining the data

g = 9.806           # acceleration of gravity
rho = 998           # water density
Ym = 3              # m upstream level
psi = 0.2           # upstream head losses
B = 3               # m, Channel width  B1 = B2
ks = 65             # m1/3s-1, strickler coefficient Ks1 = ks2;
Sb1 = 0.01          # -, bed slope
L1 = 100            # m length
Sb2 = 0.001         # -, bed slope
L2 = 100            # m length

##Defining the geometry
area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid = cn.rectangular_geometry(B)

hc = 2 * Ym / (3 + psi)
Q = area(hc) * (g * hc) ** 0.5

velocity1, discharge1 = cn.Q_chezy(area, hydraulic_radius, ks, Sb1)
f_h01 = lambda h: Q - discharge1(h)
h01 = cn.newtonR(f_h01, 1)
print("Normal depth in channel 1 is ", h01)

velocity2, discharge2 = cn.Q_chezy(area, hydraulic_radius, ks, Sb2)
f_h02 = lambda h: Q - discharge2(h)
h02 = cn.newtonR(f_h02, 1)
print("Normal depth in channel 2 is ", h02)

##Integration of profile S2 in the upstream channel
Sf = cn.energy_slope(area, hydraulic_radius, ks, Q)
Fr = cn.froude(area, hydraulic_depth, Q)

dx = 0.1
tolp = 0.01

x1 = np.empty(0)
z1 = np.empty(0)
h1 = np.empty(0)
x1 = np.append(x1, 0)
z1 = np.append(z1, 0)
h1 = np.append(h1, 0.99 * hc)

i = 0
while x1[-1] < L1:
    x1 = np.append(x1, x1[i] + dx)
    z1 = np.append(z1, z1[i] - dx * Sb1)
    h1 = np.append(h1, h1[i] + (Sb1 - Sf(h1[i])) / (1 - Fr(h1[i]) ** 2) * (x1[i + 1] - x1[i]))
    i = i + 1
# print(len(x1), len(h1), h1[-1], h1[-2])

x2 = np.empty(0)
z2 = np.empty(0)
h2 = np.empty(0)
x2 = np.append(x2, L1 + L2)
h2 = np.append(h2, 1.01 * hc)
z2 = np.append(z2, -(L1 * Sb1 + L2 * Sb2))

i = 0
while (x2[-1]) > L1:
    x2 = np.append(x2, x2[i] - dx)
    z2 = np.append(z2, z2[i] + dx * Sb2)
    h2 = np.append(h2, h2[i] - (Sb2 - Sf(h2[i])) / (1 - Fr(h2[i]) ** 2) * dx)
    i = i + 1
# print(h2[-1], len(h2))

pressure_force, momentum_flux, S = cn.specific_force(depth_centroid, area, Q, rho)
if (S(h1[-1])) < (S(h2[-1])):
    tp = 1
    print("\nS1 in the upstream channel")
else:
    tp = 2
    print("\nM3 in the downstream channel")
print(tp)

if tp == 1:

    h3 = np.empty(0)
    h3 = np.append(h3, h2[-1])

    x3 = np.empty(0)
    x3 = np.append(x3, L1)

    z3 = np.empty(0)
    z3 = np.append(z3, -L1 * Sb1)

    i = 0
    j = -1

    while (S(h3[i])) > (S(h1[j])):
        # print(i)
        x3 = np.append(x3, x3[i] - dx)
        z3 = np.append(z3, z3[i] + dx * Sb1)
        h3 = np.append(h3, h3[i] - (Sb1 - Sf(h3[i])) / (1 - Fr(h3[i]) ** 2) * dx)
        i = i + 1
        j = -1 - i

    # print(i,j)
    print("Hydraulic jump forms at ", x3[-1])
    x1c = x1[0:len(x1) + j + 1]
    h1c = h1[0:len(x1) + j + 1]
    z1c = z1[0:len(x1) + j + 1]
    # cnt = len(x1)+j # count variable to set length of plot h1

    plt.plot(x1, z1 + hc, '-', dashes=[8, 4, 8, 4, 8, 4], linewidth=0.8, color='k')
    plt.plot(x1, z1, color='k')
    plt.plot(x1, z1 + h01, '--', linewidth=1, color='b')
    plt.plot(x1c, z1c + h1c, color='r')
    plt.plot(x2, z2, color='k')
    plt.plot(x2, z2 + hc, '--', linewidth=0.8, color='k')
    plt.plot(x2, z2 + h02, '--', linewidth=1, color='b')
    plt.plot(x2, z2 + h2, color='r')
    plt.plot(x3, z3 + h3, color='r')
    plt.show()

else:
    h3 = np.empty(0)
    h3 = np.append(h3, h1[-1])

    x3 = np.empty(0)
    x3 = np.append(x3, L1)

    z3 = np.empty(0)
    z3 = np.append(z3, -L1 * Sb1)

    i = 0
    j = -1

    while (S(h3[i])) > (S(h2[j])):
        # print(i)
        x3 = np.append(x3, x3[i] + dx)
        z3 = np.append(z3, z3[i] - dx * Sb2)
        h3 = np.append(h3, h3[i] + (Sb2 - Sf(h3[i])) / (1 - Fr(h3[i]) ** 2) * dx)
        i = i + 1
        j = -1 - i

    # print(i,j)
    print("Hydraulic jump forms at ", x3[-1])
    x2c = x2[0:len(x2) + j + 1]
    h2c = h2[0:len(x2) + j + 1]
    z2c = z2[0:len(x2) + j + 1]
    # cnt = len(x1)+j # count variable to set length of plot h1

    plt.plot(x1, z1 + hc, '-', dashes=[8, 4, 8, 4, 8, 4], linewidth=0.8, color='k')
    plt.plot(x1, z1, color='k')
    plt.plot(x1, z1 + h01, '--', linewidth=1, color='b')
    plt.plot(x1, z1 + h1, color='r')
    plt.plot(x2, z2, color='k')
    plt.plot(x2, z2 + hc, '--', linewidth=0.8, color='k')
    plt.plot(x2, z2 + h02, '--', linewidth=1, color='b')
    plt.plot(x2c, z2c + h2c, color='r')
    plt.plot(x3, z3 + h3, color='r')
    plt.show()