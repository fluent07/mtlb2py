from logging import error
import math as m


def rectangular_geometry(width):
    area = lambda h: h * width
    wperimeter = lambda h: width + 2 * h
    hydraulic_radius = lambda h: area(h) / wperimeter(h)
    hydraulic_depth = lambda h: area(h) / width
    depth_centroid = lambda h: h / 2
    return area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid


def trapezoidal_geometry(minor_base,m):
    base1 = minor_base
    base2 =lambda h: 2*m*h+base1
    area =lambda h: (1/2)*(base1+base2(h))*h
    wperimeter = lambda h: base1 + (2*h*(1+m**2)**0.5)
    hydraulic_radius =lambda h: area(h)/wperimeter(h)
    hydraulic_depth =lambda h: area(h)/base2(h)
    depth_centroid =lambda h: h/3*(2*base1+base2(h))/(base1+base2(h))
    return area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid


def circular_geometry(diameter):
    radius = diameter / 2
    teta = lambda h: m.acos(1-h/radius)
    area = lambda h: radius**2 * (teta(h) - m.sin(2*teta(h))/2)
    wperimeter = lambda h: teta(h) * diameter
    hydraulic_radius = lambda h: area(h) / wperimeter(h)
    top_width = lambda h: diameter * m.sin(teta(h))
    hydraulic_depth = lambda h: area(h) / top_width(h)
    depth_centroid = lambda h: radius-2*radius*(m.sin(teta(h)))**3/3/(teta(h)-m.sin(teta(h))*m.cos(teta(h)))
    return area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid


def Q_chezy(area_q, hydraulic_radius_q, ks, Sb):
    velocity = lambda h: ks * hydraulic_radius_q(h) ** (2 / 3) * Sb ** (1 / 2)
    discharge = lambda h: area_q(h) * velocity(h)
    return velocity, discharge


def froude(area_f, hydraulic_depth_f, discharge_f):
    g = 9.806
    Fr = lambda h: discharge_f / (area_f(h) * (g * hydraulic_depth_f(h)) ** 0.5)
    return Fr


def energy(area_e, discharge_e):
    g = 9.806
    piezometric_head = lambda h: h
    velocity_head = lambda h: (discharge_e ** 2) / (2 * g * area_e(h) ** 2)
    specific_energy = lambda h: piezometric_head(h) + velocity_head(h)
    return piezometric_head, velocity_head, specific_energy


def energy_slope(area_es, hydraulic_radius_es, ks_es, discharge_es):
    Sf = lambda h: (discharge_es ** 2) / (area_es(h) ** 2 * ks_es ** 2 * (hydraulic_radius_es(h) ** (4 / 3)))
    return Sf


def specific_force(depth_centroid_sf, area_sf, discharge_sf, density_sf):
    g = 9.806
    gamma = density_sf * g      # specific weight
    pressure_force = lambda h: gamma * area_sf(h) * depth_centroid_sf(h)
    momentum_flux = lambda h: density_sf * discharge_sf ** 2 / area_sf(h)
    force = lambda h: pressure_force(h) + momentum_flux(h)
    return pressure_force, momentum_flux, force


def newtonR(f, x0):
    tolf = 1e-08  # tolerance on the value of f
    tolx = 1e-08  # tolerance of the amplitude of the [a b] interval
    dx = 1e-08
    df = lambda x: (f(x + dx) - f(x)) / dx
    iter_root = [x0]
    N = 1000
    k = 1
    while k < N:
        # print(x[0])
        iter_root.append((iter_root[k - 1]) - (f(iter_root[k - 1]) / df(iter_root[k - 1])));  # find the value of xk that makes the tangent to the function f = 0 at a certain step k
        # Check if we found a root in x(k):
        # a) yes if the value of the function f(x(k))  is less than the tolerance tolf 
        if abs(f(iter_root[k])) <= tolf:
            root = iter_root[k]
            return root
        # b) yes if the amplitude of the [x(k) x(k-1)] interval is less than the tolerance tolx
        elif abs(iter_root[k] - iter_root[k - 1]) <= tolx:
            root = iter_root[k]
            return root
        k += 1
        # 2. In case no roots were found after N iterations, print an error message
    if k == N:
        error("The method does not converge")
        # return k

        
def bisection(f, a, b):
    ##  0. Define the constants
    N = 1000            # max number of tials
    tolf = 1e-08        # tolerance on the value of f
    tolx = 1e-08        # tolerance of the amplitude of the [a b] interval
    k = 1
    # 1. Check that that neither end-point is a root and that  f(a) and f(b) have the opposite sign
    if (f(a)==0):
        root = a
        return root
    elif (f(b)==0):
        root = b
        return root
    elif (f(a)*f(b) > 0):
        error('f(a) and f(b) do not have opposite signs')
    # 2. Implement the iterative procedure 
    while k < N:
        # Find the mid-point
        c = (a + b)/2
        # Check if we found a root in c:
        # yes if the value of the function f(c) in c is less than the tolerance tolf
        # yes if the amplitude of the [a b] interval is less than the tolerance tolx
        if (abs(f(c)) <= tolf ):
            root = c
            return root
        elif (abs(a-b) <= tolx):
            root = a
            return root
        # If we did not find we should continue with:
        # [a, c] if f(a) and f(c) have opposite signs, or
        # [c, b] if f(c) and f(b) have opposite signs. 
        elif (f(c)*f(a) < 0):
            b = c
        else:
            a = c
        # 2. In case no roots were found, print an error message
        if k == N:
            error('the method did not converge')