from logging import error
# Function to compute a root of a non linear equation using bisection methodology.
# Input data:
# - name of the function f which has to define elsewhere, using lambda function*
# - a and b = extremes of the interval where to find the root of the function
def bisection(f, a, b):
    #  0. Define the constants
    N = 1000 # max number of tials
    tolf = 1e-08 # tolerance on the value of f
    tolx = 1e-08 # tolerance of the amplitude of the [a b] interval
    k = 1
    # 1. Check that that neither end-point is a root and that  f(a) and f(b) have the opposite sign
    if (f(a)==0):
        root = a
        return root
    elif (f(b)==0):
        root = b
        return root
    elif (f(a)*f(b) > 0):
        error('f(a) and f(b) do not have opposite signs')

    # 2. Implement the iterative procedure 
    while k < N:
        # Find the mid-point
        c = (a + b)/2
        # Check if we found a root in c:
        # yes if the value of the function f(c) in c is less than the tolerance tolf
        # yes if the amplitude of the [a b] interval is less than the tolerance tolx
        if (abs(f(c)) <= tolf ):
            root = c
            return root

        elif (abs(a-b) <= tolx):
            root = a
            return root      

        # If we did not find we should continue with:
        # [a, c] if f(a) and f(c) have opposite signs, or
        # [c, b] if f(c) and f(b) have opposite signs. 

        elif (f(c)*f(a) < 0):
            b = c
        else:
            a = c
        # 2. In case no roots were found, print an error message
        if k == N:
            error('the method did not converge')


import numpy as np
import matplotlib.pyplot as plt
import test as cn

## 1. Definition of the functions describing the geometrical characteristics of the river

Q = 6           # discharge m^3/s
ks = 40         # Strickler coefficient m^(1/3)/s
Sb = 0.01       # bed slope

# rectangular cross section
B = 5           # width (m)
area, wperimeter, hydraulic_radius, hydraulic_depht, depth_centroid = cn.rect(B)

# # trapezoidal cross section
# b = 3
# m = 1
# area_T, wperimeter_T, hydraulic_radius_T, hydraulic_depth_T, depth_centroid_T = trapezoid_geometry(b, m)

## 2. Calculation of the stage-discharge relationship
# Recall the Q_chezy relationship
velocity, discharge = cn.Q_chezy(area, hydraulic_radius, ks, Sb)
# [discharge_T, velocity_T] = cn.Q_chezy(area_T, hydraulic_radius_T, ks, Sb)

# First step: to be applied to a single value discharge only
f =lambda h: Q-discharge(h)     # Define the object function f (We want to find the roots of f)
h_newton = cn.newtonR(f,2)       # Find the root of f
h_bisection = bisection(f,-10,5)

# Second step: to be applied to a set of discharges
Q = np.arange(0,151,1)
i = 0
h_newton = np.empty(0)
h_bisection = np.empty(0)
while i < len(Q):
  f =lambda h: Q(i)-discharge(h)
  h_newton = np.append(h_newton,cn.newtonR(f,2))
  h_bisection = np.append(h_bisection,cn.bisection(f, 0, 10))
  i +=1

plt.figure(1)
plt.subplot(2,3,1)
plt.plot(h_newton,Q,'b')
#plot(h_bisection,Q,'.k')


plt.subplot(2,3,4)
plt.plot(h_newton,velocity(h_newton),'b')
plt.plot(h_bisection,velocity(h_bisection),'k')
plt.xlabel('Height (m)')
plt.ylabel('Discharge (m^3/s)')