# Classwork 1.2
# Representation of the stage-discharge curve Q(Y) curves for a given cross section.
# required packages
import numpy as np
import matplotlib.pyplot as plt
import functions as cn

# 1. Definition of the geometrical characteristics of the river

hr = np.arange(0, 5.1, 0.01) # water depth vector
ks = 40                      # Strickler coefficient m^(1/3)/s
Sb = 0.01                    # bed slope
# rectangular cross section
B = 5                        # width (m)

# 2. Call of the required functions

area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid = cn.rectangular_geometry(B) # recall the geometrical functions
velocity, discharge = cn.Q_chezy(area, hydraulic_radius, ks, Sb) # recall the Chezy function

# 3. Calculation and representation of the stage-discharge relationship for a given depth vector hr

plt.figure(1)

plt.subplot(1,2,1)
plt.plot(hr,discharge(hr))
plt.title("Stage-Discharge Curve") # Title for subplot

plt.subplot(1,2,2)
plt.plot(hr,velocity(hr))
plt.title("Stage-Velocity Curve") # Title for subplot

plt.suptitle("Classwork 1.2") # Title for whole plot
plt.show()

