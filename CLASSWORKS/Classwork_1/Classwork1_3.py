## Classwork 1.3
# Representation of the specific energy E(Y) and specific force S(Y) curves for a given cross section.
import numpy as np
import matplotlib.pyplot as plt
import functions as cn
## 1. Definition of the geometrical characteristics of the river

rho = 1000 # density kg/m3

# rectangular cross section
b = 5 # width (m)

# Discharge and water depths vector
Q = 6                       # reference discharge for E and S curves (m^3/2)
discharge2 = 10
hr = np.arange(0.01,5,0.01) # water depth vector: from 0.01 to 5 spaced every 0.01

## 2. Call of the required functions
area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid = cn.rectangular_geometry(b)   #recall the rectangular geometry function
piezometric_head, velocity_head, E = cn.energy(area, Q)                            #recall the energy script function
pressure_force, momentum_flux, S = cn.specific_force(depth_centroid, area, Q, rho) #recall the specific force function
Fr = cn.froude(area, hydraulic_depth, Q)                                           #recall the froude number function

# [piezometric_head2, velocity_head2, E2] = energy(area, discharge2);                               #recall the energy script function
# [pressure_force2, momentum_flux2, S2] = specific_force(depth_centroid, area, discharge2, rho);    #recall the specific force function
# [Fr2] = froude(area, hydraulic_depth, discharge2);                                                #recall the froude number function

## 3. Representation of the specific energy E(Y), the specific force S(Y) and Fr(Y) for a given depth vector hr

# Commands to find the location of the minimum value of E and S
# index_ycrE = find(E(hr) == min(E(hr)));
min_E = min(E(hr))
f2 = lambda x: E(x)-min_E
h_min_E = cn.newtonR(f2,3)

min_S = min(S(hr))
f3 = lambda x: S(x)-min_S
h_min_S = cn.newtonR(f3,3)

min_Fr = min(Fr(hr))
f4 = lambda x: Fr(x)-min_Fr
h_min_Fr = cn.newtonR(f4,3)
# index_ycrS = find(force(hr) == min(force(hr)));
# index_ycrFr = find(Fr(hr)==min(Fr(hr)));

plt.figure(1)

# first subplot: specific energy E
plt.subplot(1,3,1)
plt.plot(hr, piezometric_head(hr),'b--') # plot the piezometric head
plt.plot(hr, velocity_head(hr),'g--') # plot the velocity head
plt.plot(hr, E(hr),'r',linewidth=2) # plot the energy. Put the thickess equal to 2
plt.xlabel('Water depth (m)')
plt.ylabel('Specific energy (m)')
plt.ylim(0,5)
plt.minorticks_on()
plt.grid(visible=True, which='major', color='k', linestyle='-')
plt.grid(visible=True, which='minor', color='k', linestyle='-', alpha=0.2)
# plot a dot indicating the minimum of E
plt.plot(h_min_E, min_E, marker = 'o', fillstyle = 'none')


# # second subplot: specific force S
plt.subplot(1,3,2)
# plt.plot(x12_p3, z12_p3, color='k', linewidth=0.8)
plt.plot(hr, piezometric_head(hr),'b--', linewidth = '0.5') # plot the piezometric head
plt.plot(hr, velocity_head(hr),'g--', linewidth = '0.5') # plot the velocity head
plt.plot(hr, S(hr),'r',linewidth='2') # plot the energy. Put the thickess equal to 2
plt.xlabel('Water depth (m)')
plt.ylabel('Specific force (N)')
plt.minorticks_on()
plt.grid(visible=True, which='major', color='k', linestyle='-')
plt.grid(visible=True, which='minor', color='k', linestyle='-', alpha=0.2)

# plot a dot indicating the minimum of S
plt.plot(h_min_S, min_S, marker = 'o', fillstyle = 'none')


# # third subplot: Froude number
plt.subplot(1,3,3)
# plot(hr, piezometric_head(hr),'--b') # plot the piezometric head
# plot(hr, velocity_head(hr),'--g') # plot the velocity head
plt.plot(hr, Fr(hr),'r',linewidth = '2') # plot the energy. Put the thickess equal to 2
plt.xlabel('Water depth (m)')
plt.ylabel('Froude')
plt.ylim(0,5)
plt.minorticks_on()
# plt.grid()
plt.grid(visible=True, which='major', color='k', linestyle='-')
plt.grid(visible=True, which='minor', color='k', linestyle='-', alpha=0.2)

# plot a dot indicating the minimum of S
plt.plot(h_min_S, Fr(h_min_S), marker = 'o', color = 'r', fillstyle = 'none')
# plt.scatter(h_min_S, Fr(h_min_S), marker = 'o', facecolors = 'none', edgecolors = 'r' )
plt.show()
