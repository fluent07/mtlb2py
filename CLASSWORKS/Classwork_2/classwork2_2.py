## Classwork 2
# Representation of the stage-discharge curve Q(Y) curves for a given cross
# section by finding the roots of the Chezy Equation
import numpy as np
import matplotlib.pyplot as plt
import test as cn

## 1. Definition of the functions describing the geometrical characteristics of the river
Q1 = 6           # discharge m^3/s
ks = 40         # Strickler coefficient m^(1/3)/s
Sb = 0.01       # bed slope

# rectangular cross section
B = 5           # width (m)
area, wperimeter, hydraulic_radius, hydraulic_depht, depth_centroid = cn.rectangular_geometry(B)
# trapezoidal cross section
b = 3
m = 1
area_T, wperimeter_T, hydraulic_radius_T, hydraulic_depth_T, depth_centroid_T = cn.trapezoidal_geometry(b, m)

## 2. Calculation of the stage-discharge relationship
# Recall the Q_chezy relationship
velocity, discharge = cn.Q_chezy(area, hydraulic_radius, ks, Sb)
velocity_T, discharge_T = cn.Q_chezy(area_T, hydraulic_radius_T, ks, Sb)

# First step: to be applied to a single value discharge only
f = lambda h: Q1 - discharge(h)     # Define the object function f (We want to find the roots of f)
h_newton_1 = cn.newtonR(f,2)       # Find the root of f
# h_bisection = cn.bisection(f,-10,5)

# Second step: to be applied to a set of discharges
Q = np.arange(0,151,1)
i = 0
h_newton = np.empty(0)
h_bisection = np.empty(0)
while i < len(Q):
  f =lambda h: Q[i]-discharge(h)
  h_newton = np.append(h_newton,cn.newtonR(f,2))
  i +=1

i = 0
h_newton_T = np.empty(0)
h_bisection_T = np.empty(0)
while i < len(Q):
  f =lambda h: Q[i]-discharge_T(h)
  h_newton_T = np.append(h_newton_T,cn.newtonR(f,2))
  i +=1

# # for i = 1:length(Q) 
# #   f_T =lambda h: Q(i)-discharge_T(h); 
# #   h_newton_T(i) = newton(f_T,2);
# #   h_bisection_T(i) = bisection(f_T, 0, 10);
# # end
# # Third step: 
# for i = 1:length(Q) 
#   f =lambda h: Q(i)-discharge(h); 
#   [h_newton(i), newton_iter(i)] = newton_iter(f,2);
#   [h_bisection(i), k(i)] = bisection_iter(f, 0, 10);
# end
# for i = 1:length(Q) 
#   f_T =lambda h: Q(i)-discharge_T(h); 
#   [h_newton_T(i), newton_iter_T(i)] = newton_iter(f_T,2);
#   [h_bisection_T(i), bisec_iter_T(i)] = bisection_iter(f_T, 0, 10);
# end

## 3. Representation of the the stage-discharge relationship and comparison of the performances of the methods

# Stage discharge with the two methods
plt.figure(1)
plt.subplot(2,3,1)
plt.plot(h_newton,Q,'b')
plt.xlabel('Height (m)')
plt.ylabel('Discharge (m^3/s)')

plt.subplot(2,3,2)
plt.plot(h_newton_T,Q,'b')
plt.xlabel('Height (m)')
plt.ylabel('Discharge (m^3/s)')
# plt.legend()
# subplot(2,3,2)
# hold on
# plot(h_newton_T,Q,'b')
# plot(h_bisection_T,Q,'.r')
# xlabel('Height (m)')
# ylabel('Discharge (m^3/s)')
# grid on
# grid minor
# legend('Newthon Raphson method', 'Bisection method')

# Stage velocity with the two methods

plt.subplot(2,3,4)
plt.plot(h_newton,velocity(h_newton),'b')
# plt.plot(h_bisection,velocity(h_bisection),'k')
plt.xlabel('Height (m)')
plt.ylabel('Velocity (m/s)')

plt.subplot(2,3,5)
plt.plot(h_newton_T,velocity(h_newton_T),'b')
# plt.plot(h_bisection,velocity(h_bisection),'k')
plt.xlabel('Height (m)')
plt.ylabel('Velocity (m/s)')
plt.show()
# legend('Newthon Raphson method', 'Bisection method')
# subplot(2,3,5)
# hold on
# plot(h_newton_T,velocity(h_newton_T),'b')
# plot(h_bisection_T,velocity(h_bisection_T),'.r')
# xlabel('Height (m)')
# ylabel('Discharge (m^3/s)')
# grid on
# grid minor
# legend('Newthon Raphson method', 'Bisection method')


# Comparison of the number of iterations
# subplot(2,3,3)
# hold on
# plot(Q, newton_iter,'k')
# plot(Q, bisec_iter,'r')
# xlabel('Discharge')
# ylabel('Iterations')
# grid on
# grid minor

# Display the mean number of iterations with the 2 methodologies
# disp('Newthon method: ')
# disp(mean(n_iter_newton))
# disp('Bisection method: ')
# ...
