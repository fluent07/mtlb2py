## Classwork 2
# script for balancing the energy equation across a sluice gate in a rectangular cross section. 
import numpy as np
import matplotlib.pyplot as plt
import test as cn

## 1. Definition of the functions describing the geometrical characteristics of the river
# rectangular channel
B = 5           # Channel width 
Q = 6           # mc/s  discharge 
a = 0.35        # m  Sluice gate opening on the channel bottom (area)
Cc = 0.611015   # Contraction coefficient 
dH = 0          # m  Localized head loss  

area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid = cn.rect(B)

## 2. Definition of the energy functions and root search

piezometric_head, velocity_head, E = cn.energy(area, Q) # recall the energy function
f = lambda h: E(a*Cc)- E(h)# Define the object function f (We want to find the roots of f)
solution1 = cn.newtonR(f,3) # Find the root of f on the basis of the newthon method
solution2 = cn.bisection(f,-1,4) # Find the root of f on the basis of the bisection method

print('Water depth upstream the gate with the Newthon method(m):')
print(solution1)

print('Water depth upstream the gate with the Bisection method(m):')
print(solution2)