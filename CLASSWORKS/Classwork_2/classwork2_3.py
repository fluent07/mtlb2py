# # Classwork 2
# script for computing the quantities of interest across an hydraulic jump.
import test as cn
# # 1. Definition of the functions describing the geometrical characteristics of the river
# rectangular channel
B = 3           # m, Channel width
Q = 26.6        # mc/s,  discharge
Sb = 0.01       # -, bed slope
density = 998   # water density
ks = 65         # m1/3s-1 strickler coefficient

area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid = cn.rectangular_geometry(B)

# # 2. Calculation and visualization of the normal depth
velocity, discharge = cn.Q_chezy(area, hydraulic_radius, ks, Sb)  # recall the Chezy equation

# calculation of the normal depth:
f = lambda h: Q - discharge(h)
ND = cn.newtonR(f, 5)
# visualization of the result
print("Normal Depth (m):")
print(ND)

# # 3. Calculation and visualization of the conjugate depth
pressure_force, momentum_flux, S = cn.specific_force(depth_centroid, area, Q, density)
f2 = lambda h: S(ND) - S(h)
CD = cn.newtonR(f2, 5)
print("conjugate depth(m)")
print(CD)

# # 4. Calculation of the energy loss across the hydraulic jump
piezometric_head, velocity_head, E = cn.energy(area, Q) # recall the energy equation.

DE = E(ND) - E(CD) # calculation of the energy loss.

# visualization of the result
print("Difference in energy (m):")
print(DE)
