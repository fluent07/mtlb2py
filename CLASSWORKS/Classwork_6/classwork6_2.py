## Classwork 6
# script for computing the profile in a channel with increasing sicharge

import numpy as np
import matplotlib.pyplot as plt
import test as cn

## 1. Definition of the functions describing the geometrical characteristics of the canal

g = 9.806           #acceleration of gravity
rho = 998           #water density

B = 2               # m, channel width  
ks =  50            # m1/3s-1, strickler coefficient 
Sb = 0.001          # -, bed slope
L = 30              #m length of the weir

Q = 5.3             #downstream discharge

area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid = cn.rectangular_geometry(B)

## 2. Calculation of the critical depth (hc2) and the normal depth (h02) downstream the weir
Fr = cn.froude(area, hydraulic_depth, Q)
fhc2 = lambda h: Fr(h)-1
hc2 = cn.newtonR(fhc2,1)

velocity, discharge = cn.Q_chezy(area, hydraulic_radius, ks, Sb)
fh02 = lambda h: Q - discharge(h)
h02 = cn.newtonR(fh02, 2)

    
if h02>hc2:
    print('The downstream channel is a mild slope channel  - OK')

else:
    print('Attention!!! The channel is a steep slope channel - CHANGE THE PROCEDURE')


## 3. Integration of the profile above the weir

# Boundary conditions
dx = 0.1; # spatial path at each step
x2 = np.array([L])
zb2 = np.array([L*Sb])
Q2 = np.array([Q])
h2 = np.array([hc2])
i = 0
dq = Q/L
pressure_force, momentum_flux, S = cn.specific_force(depth_centroid, area, Q, rho)
Scont = S(hc2)

# integration of the profile x2, h2
while min(x2)>0:
   x2 = np.append(x2, x2[i] - dx)
   zb2 = np.append(zb2, zb2[i]+dx*Sb)
   Q2 = np.append(Q2, Q2[i]- dq*dx)
   pressure_force, momentum_flux, Sm = cn.specific_force(depth_centroid, area, Q2[i+1], rho)
   f2 =lambda h: Scont-Sm(h)
   h2 = np.append(h2, cn.newtonR(f2,h2[i]))
   i = i+1

## 4. Representation of the profiles
plt.figure(1)
# Water elevation
plt.subplot(1,2,1)
plt.plot(x2,zb2,'k')
plt.plot(x2,zb2+h2,'r')
plt.xlabel('Station (m)') # add the label of the x axis
plt.xlim(0, L)
plt.ylabel('Water elevation (m)') # add the label of the y axis

 
 
# Discharge
plt.subplot(1,2,2)
plt.plot(x2,Q2)
plt.xlabel('Station (m)') # add the label of the x axis
plt.xlim(0, L)
plt.ylabel('Discahrge (m^3/s)') # add the label of the y axis
plt.show()
