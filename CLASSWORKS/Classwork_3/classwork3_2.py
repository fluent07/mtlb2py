## Classwork 3.2
# script for computing the backwater effect caused by the elevation level of Iseo lake on the tributary Oglio river

import numpy as np
import matplotlib.pyplot as plt
import test as cn

## 1. Definition of the functions describing the geometrical characteristics of the river
# trapeze channel

b = 75              # m, Channel width 
m = 4.6             # m/m base angle
Q = 55              # mc/s,  discharge 
Sb = 0.001          # -, bed slope
ks = 33             # m1/3s-1, strickler coefficient

area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid = cn.trapezoidal_geometry(b, m) # Recall trapeze geometry
    
## 2. Calculation of the normal depth (h0), the critical depth (hc) and of the lake's level (hG)
velocity, discharge = cn.Q_chezy(area, hydraulic_radius, ks, Sb)
fr = cn.froude(area, hydraulic_depth, Q)
f1 = lambda h: Q-discharge(h)
f2 = lambda h: 1-fr(h)
h0 = cn.newtonR(f1,2)
hc = cn.newtonR(f2,0.5)

hG = np.arange(0.7*h0,3.1*h0,0.1*h0)
# print(len(hG))

## 3. Integration of the profile (xP,hP) with the standard step method
piezometric_head, velocity_head, energy = cn.energy(area, Q)
Sf = cn.energy_slope(area, hydraulic_radius, ks, Q)
Fr = cn.froude(area, hydraulic_depth, Q)

dx = 1          # spatial path at each step
tolP = 0.01     # tolerance 

plt.figure(1)
plt.xlabel('Station (m)')           # add the label of the x axis
plt.ylabel('Water elevation (m)')   # add the label of the y axis

# Cicle that investiage all the boundary conditions

# Definition of the boundary condition under analysis
j = 0
while j<len(hG):
    hP = np.array([hG[j]])
    xP = np.array([0])
    z_bottom = np.array([0])
    # Calculation of the profile with the explicit standard step. 
    i = 0
    while abs(hP[-1]-h0)/hP[-1] > tolP:
        xP = np.append(xP, xP[i] - dx)
        z_bottom = np.append(z_bottom, z_bottom[i] + dx*Sb)
        hP = np.append(hP, hP[i] + (Sb-Sf(hP[i]))/(1-Fr(hP[i])**2) * (xP[i+1]-xP[i]))
        i=i+1   

    plt.plot(xP,z_bottom,'-k','LineWidth',1.5)
    plt.plot(xP,z_bottom+h0,'--b','LineWidth',0.5)
    plt.plot(xP,z_bottom+hc,'--k','LineWidth',0.5)
    plt.plot(xP,z_bottom+hP,'r','LineWidth',2)
        
    j = j+1	

plt.show()






