## Classwork 3.1
# script for computing the M1 profile upstream a gate using a standard step
# method

import numpy as np
import matplotlib.pyplot as plt
import test as cn

## 1. Definition of the functions describing the geometrical characteristics of the river
# rectangular channel

B = 5                  # m, Channel width 
Q = 30                 # mc/s,  discharge 
Sb = 0.019             # -, bed slope
ks = 90                # m1/3s-1, strickler coefficient
a = 0.900              # m, opening heigth
Cc = 0.61              # -, contraction coefficient
#hG = 0.18             # Depth upstream the sluice gate
density = 1000  
area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid = cn.rectangular_geometry(B)

## 2. Calculation of the normal depth (h0) and the critical depth (hc)

velocity, discharge = cn.Q_chezy(area, hydraulic_radius, ks, Sb); 
f1 = lambda h: Q-discharge(h)
h0 = cn.newtonR(f1,2)
hc = (Q**2/9.81/B**2)**(1/3)
print('Normal depth')
print(h0)
print('Critical depth')
print(hc)

## 3. Calculation of the depth immediatly upstream the gate (hG)
piezometric_head, velocity_head, E = cn.energy(area, Q)
f3 = lambda h: E(a*Cc)-E(h)
hG = cn.newtonR(f3,3)

print('Depth upstream the gate')
print(hG)

## 4. Integration of the profile (xP,hP) with the standard step method

Sf = cn.energy_slope(area, hydraulic_radius, ks, Q); # recall the energy slope function Sf(h)
Fr = cn.froude(area, hydraulic_depth, Q); # recall the froude number function Fr(h)
pressure_force, momentum_flux, S = cn.specific_force(depth_centroid, area, Q, density);

# Boundary conditions
xP = np.array([0])
z_bottom = np.array([0])
hP = np.array([hG])

dx = 0.01              # spatial path at each step
tolP = 0.01            # tolerance on the water level: I will stop the computation when I will reach h0

# explicit formulation
i=0
if h0<hc:
   while S(hP[-1]) > S(h0):
        xP = np.append(xP, xP[i] - dx)
        z_bottom = np.append(z_bottom, z_bottom[i] + dx*Sb)
        hP = np.append(hP, hP[i] + (Sb-Sf(hP[i]))/(1-Fr(hP[i])**2) * (xP[i+1]-xP[i]))
        i=i+1  
   
   print('the location of Hydraulic jump is at');
   print(xP[-1])

if h0>hc:
    while abs(hP[-1]-h0)/hP[-1] > tolP:
        xP = np.append(xP, xP[i] - dx)
        z_bottom = np.append(z_bottom, z_bottom[i] + dx*Sb)
        hP = np.append(hP, hP[i] + (Sb-Sf(hP[i]))/(1-Fr(hP[i])**2) * (xP[i+1]-xP[i]))
        i=i+1    
    print('the location of profile is starting at');
    print(xP[-1])

## 5. Plot
plt.figure(1)
plt.plot(xP,z_bottom,'-k')
plt.plot(xP,z_bottom+h0,'--b')
plt.plot(xP,z_bottom+hc,'--k')
plt.plot(xP,z_bottom+hP,'r')
plt.xlabel('Station (m)',) # add the label of the x axis
plt.ylabel('Water elevation (m)') # add the label of the y axis
plt.show()
