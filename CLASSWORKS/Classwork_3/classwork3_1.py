## Classwork 3.1
# script for computing the M1 profile upstream a gate using a standard step method

import numpy as np
import matplotlib.pyplot as plt
import test as cn

## 1. Definition of the functions describing the geometrical characteristics of the river
# rectangular channel
B = 10             # m, Channel width 
Q = 15             # mc/s,  discharge 
Sb = 0.0005        # -, bed slope
ks = 45            # m1/3s-1, strickler coefficient
a = 0.405          # m, opening heigth
Cc = 0.61          # -, contraction coefficient

area, wperimeter, hydraulic_radius, hydraulic_depth, depth_centroid = cn.rectangular_geometry(B)

## 2. Calculation of the normal depth (h0) and the critical depth (hc)
velocity, discharge = cn.Q_chezy(area, hydraulic_radius, ks, Sb) #call chezy function
f_h0 = lambda h:discharge(h)-Q
h0 = cn.newtonR(f_h0,3) # Normal Depth in the channel
Fr = cn.froude(area, hydraulic_depth, Q) #call Froude function
f_hc= lambda h:Fr(h)-1
hc = cn.newtonR(f_hc,1) # Critical Depth in the channel
print('Normal depth')
print(h0)
print('Critical depth')
print(hc)
if (h0>hc):
    print('the channel is mild slope channel')

if (h0==hc):
    print('The channel is critical slope channel')

if (h0<hc):
    print('It is a steep slope channel')

## 3. Calculation of the depth immediatly upstream the gate (hG)
piezometric_head, velocity_head, E = cn.energy(area, Q)
f_hG = lambda h:E(h)-E(a*Cc)
hG = cn.newtonR(f_hG,3)

print('Depth upstream the gate')
print(hG)

## 4. Integration of the profile (xP,hP) with the standard step method
Sf = cn.energy_slope(area, hydraulic_radius, ks, Q) # recall the energy slope function Sf(h)
#[Fr] = froude(area, hydraulic_depth, Q) # recall the froude number function Fr(h)

# Boundary conditions
xP = np.array([0])
z_bottom = np.array([0])
hP = np.array([hG])
dx = 1;             # spatial path at each step
tolP = 0.01;        # tolerance on the water level: I will stop the computation when I will reach h0

# explicit formulation
i = 0
while abs(hP[-1]-h0)/hP[-1] > tolP:
    xP = np.append(xP, xP[i] - dx)
    z_bottom = np.append(z_bottom, z_bottom[i] + dx*Sb)
    hP = np.append(hP, hP[i] + (Sb-Sf(hP[i]))/(1-Fr(hP[i])**2) * (xP[i+1]-xP[i]))
    i=i+1

## 5. Plot
plt.figure(1)
plt.plot(xP,z_bottom,'k', linewidth = '1')
plt.plot(xP,z_bottom+hc,'-k', linewidth = '0.5')
plt.plot(xP,z_bottom+h0,'b-', linewidth = '0.5')
plt.plot(xP,z_bottom+hP,'r', linewidth = '2')
plt.xlabel('Station (m)')               # add the label of the x axis
plt.ylabel('Water elevation (m)') 
plt.show()                              # add the label of the y axis